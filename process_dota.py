from FuncMl import FuncMl
from openpyxl.styles import Alignment
import openpyxl
import cv2
import os


class ProcessDOTA:

    def __init__(self):
        self.class_func = FuncMl()

    def get_info(self, img, ocr_json, debug=False):
        ret = {}
        # ------------------------ detect the region of keys ----------------------------
        pos_duration, pos_victory, pos_replay, pos_overview = None, None, None, None
        for i in range(1, len(ocr_json)):
            if ocr_json[i]['description'] == 'DURATION':
                pos_duration = self.class_func.get_rect_ocr_data(ocr_json, i)

            if ocr_json[i]['description'] == 'Victory':
                pos_victory = self.class_func.get_rect_ocr_data(ocr_json, i)

            if ocr_json[i]['description'] == 'REPLAY':
                pos_replay = self.class_func.get_rect_ocr_data(ocr_json, i)

            if ocr_json[i]['description'] == 'OVERVIEW':
                pos_overview = self.class_func.get_rect_ocr_data(ocr_json, i)

        # -------------------------- extract individual info -----------------------------
        if pos_duration is not None and pos_overview is not None and pos_replay is not None:
            x1 = pos_overview[0] + (pos_overview[2] - pos_overview[0]) * 0.33
            x2 = pos_duration[0] + (pos_duration[2] - pos_duration[0]) * 0.25
            y1 = pos_duration[3] + pos_duration[1] - pos_overview[3]
            y2 = int(pos_replay[1] - (pos_replay[1] - pos_duration[3]) * 0.26)
            x3 = pos_duration[2] - (pos_duration[2] - pos_duration[0]) * 0.12
            x4 = pos_replay[2] + (pos_replay[2] - pos_replay[0]) * 0.5
        elif pos_overview is not None and pos_replay is not None:
            x1 = pos_overview[0] + (pos_overview[2] - pos_overview[0]) * 0.33
            x2 = pos_overview[0] + (pos_replay[2] - pos_overview[0]) * 0.51
            y1 = int(pos_overview[1] + (pos_replay[3] - pos_overview[1]) * 0.2)
            y2 = int(pos_overview[1] + (pos_replay[3] - pos_overview[1]) * 0.75)
            x3 = pos_overview[0] + (pos_replay[2] - pos_overview[0]) * 0.545
            x4 = pos_replay[2] + (pos_replay[2] - pos_replay[0]) * 0.5
        else:
            x1, x2, x3, x4, y1, y2 = 0, 0, 0, 0, 0, 0

        rect_players = []
        for i in range(5):
            rect_players.append([int(x1 + i * (x2 - x1) / 5), y1, int(x1 + (i + 1) * (x2 - x1) / 5), y2])

        for i in range(5):
            rect_players.append([int(x3 + i * (x4 - x3) / 5), y1, int(x3 + (i + 1) * (x4 - x3) / 5), y2])

        info_players = []
        for i in range(10):
            player_name = ''
            player_money = ''
            player_score = ''
            for j in range(1, len(ocr_json)):
                ocr_rect = self.class_func.get_rect_ocr_data(ocr_json, j)
                if self.class_func.check_contain_rects(rect_players[i], ocr_rect):
                    if ocr_rect[1] - rect_players[i][1] < (rect_players[i][3] - rect_players[i][1]) / 10:
                        player_name += ocr_json[j]['description'] + ' '
                    elif (rect_players[i][3] - rect_players[i][1]) * 0.8 <= ocr_rect[1] - rect_players[i][1] < \
                            (rect_players[i][3] - rect_players[i][1]) * 0.9:
                        player_money += ocr_json[j]['description'] + ' '
                    elif (rect_players[i][3] - rect_players[i][1]) * 0.9 <= ocr_rect[1] - rect_players[i][1]:
                        player_score += ocr_json[j]['description'] + ' '

            player_score = player_score.replace('T', '/').replace('B', '8')
            if player_score.count('1') + player_score.count('/') == 2:
                player_score = player_score.replace('1', '/')

            player_score_list = player_score.replace(' ', '').split('/')
            if len(player_score_list) == 2 and len(player_score_list[1]) >= 4 and player_score_list[1][2] == '1':
                player_score_list.append(player_score_list[1][3:])
                player_score_list[1] = player_score_list[1][:2]
            elif len(player_score_list) == 2 and len(player_score_list[0]) >= 4 and player_score_list[0][2] == '1':
                player_score_list.insert(1, player_score_list[0][3:])
                player_score_list[0] = player_score_list[0][:2]
            elif len(player_score_list) == 2 and len(player_score_list[1]) >= 3 and player_score_list[1][1] == '1':
                player_score_list.append(player_score_list[1][2:])
                player_score_list[1] = player_score_list[1][0]
            elif len(player_score_list) == 2 and len(player_score_list[0]) >= 3 and player_score_list[0][1] == '1':
                player_score_list.insert(1, player_score_list[0][2:])
                player_score_list[0] = player_score_list[0][0]

            info_players.append({'name': player_name.replace('[ ', '[').replace(' ]', ']').strip(),
                                 'money': player_money.strip(),
                                 'kill': player_score_list[0],
                                 'death': player_score_list[1],
                                 'assist': player_score_list[2]
                                 })

        ret['players'] = info_players

        # --------------------------- extract match info ---------------------------------
        if pos_victory is None:
            ret['victory'] = -1
        else:
            if pos_duration is not None:
                if pos_duration[2] < pos_victory[0]:
                    ret['victory'] = 1
                else:
                    ret['victory'] = 0
            elif pos_replay is not None:
                if pos_replay[0] - (pos_replay[2] - pos_replay[0]) * 5 < pos_victory[0]:
                    ret['victory'] = 1
                else:
                    ret['victory'] = 0
            else:
                if 1000 < pos_victory[0]:
                    ret['victory'] = 1
                else:
                    ret['victory'] = 1

        rect_score1 = [int((rect_players[3][0] + rect_players[3][2]) / 2),
                       rect_players[3][1] - int((rect_players[3][3] - rect_players[3][1]) * 0.25),
                       rect_players[4][2] - 30,
                       rect_players[3][1] - 10]
        rect_score2 = [rect_players[5][0] + 30,
                       rect_players[3][1] - int((rect_players[3][3] - rect_players[3][1]) * 0.25),
                       int((rect_players[6][0] + rect_players[6][2]) / 2),
                       rect_players[6][1] - 10]
        rect_time = [int((rect_players[4][0] + rect_players[4][2]) / 2),
                     rect_players[3][1] - int((rect_players[3][3] - rect_players[3][1]) * 0.2),
                     int((rect_players[5][0] + rect_players[5][2]) / 2),
                     rect_players[3][1] - 10]
        rect_team1 = [rect_players[0][0],
                      rect_players[3][1] - int((rect_players[3][3] - rect_players[3][1]) * 0.25),
                      rect_players[4][0],
                      rect_players[3][1] - 10]
        rect_team2 = [rect_players[6][0],
                      rect_players[3][1] - int((rect_players[3][3] - rect_players[3][1]) * 0.25),
                      rect_players[9][2],
                      rect_players[3][1] - 10]

        ret["duration"] = ''
        ret["team1"] = ''
        ret['team2'] = ''
        for i in range(1, len(ocr_json)):
            ocr_rect = self.class_func.get_rect_ocr_data(ocr_json, i)
            if self.class_func.check_contain_rects(rect_score1, ocr_rect) and ocr_json[i]['description'].isdigit():
                ret["score1"] = ocr_json[i]['description']
            elif self.class_func.check_contain_rects(rect_score2, ocr_rect) and ocr_json[i]['description'].isdigit():
                ret["score2"] = ocr_json[i]['description']
            elif self.class_func.check_contain_rects(rect_time, ocr_rect):
                if ocr_json[i]['description'].replace(':', '').isdigit() and ':' in ocr_json[i]['description']:
                    ret["duration"] = ocr_json[i]['description']
                elif ocr_json[i]['description'].isdigit() or ocr_json[i]['description'] == ':':
                    ret["duration"] += ocr_json[i]['description']
            elif self.class_func.check_contain_rects(rect_team1, ocr_rect):
                ret['team1'] += ocr_json[i]['description'] + ' '
            elif self.class_func.check_contain_rects(rect_team2, ocr_rect):
                ret['team2'] += ocr_json[i]['description'] + ' '

        ret['team1'] = ret['team1'].strip().rstrip('Victory').strip()
        ret['team2'] = ret['team2'].strip().rstrip('Victory').strip()
        if ret['team1'] == 'Radiant':
            ret['team1'] = ''
        if ret['team2'] == 'Dire':
            ret['team2'] = ''

        # ---------------------------------- draw result ----------------------------------
        if debug:
            for i in range(len(rect_players)):
                cv2.rectangle(img, tuple(rect_players[i][:2]), tuple(rect_players[i][2:]), (0, 255, 0), 2)

            cv2.rectangle(img, tuple(rect_score1[:2]), tuple(rect_score1[2:]), (0, 255, 0), 2)
            cv2.rectangle(img, tuple(rect_score2[:2]), tuple(rect_score2[2:]), (0, 255, 0), 2)
            cv2.rectangle(img, tuple(rect_time[:2]), tuple(rect_time[2:]), (0, 255, 0), 2)
            cv2.rectangle(img, tuple(rect_team1[:2]), tuple(rect_team1[2:]), (0, 0, 255), 2)
            cv2.rectangle(img, tuple(rect_team2[:2]), tuple(rect_team2[2:]), (0, 0, 255), 2)

            # cv2.imshow('ret', img)
            # cv2.waitKey()

        return ret

    @staticmethod
    def save_excel(excel_file, game_info_list, sheet_title):
        # ------------------ read excel file and add sheet --------------------
        if not os.path.isfile(excel_file):
            wb = openpyxl.Workbook()
            sheet_names = wb.sheetnames
            sheet = wb[sheet_names[0]]
            sheet.title = sheet_title
        else:
            wb = openpyxl.load_workbook(excel_file)
            sheet = wb.create_sheet(sheet_title)

        # ----------------------- write data to sheet ------------------------
        for game_ind in range(len(game_info_list)):
            game_info = game_info_list[game_ind]
            sr = game_ind * 10

            # ----------------- title --------------------
            sheet.merge_cells(start_row=sr + 1, start_column=1, end_row=sr + 1, end_column=7)
            sheet.cell(row=sr + 1, column=1).value = "Winning Team"
            sheet.cell(row=sr + 1, column=1).alignment = Alignment(horizontal='center')
            sheet.cell(row=sr + 1, column=8).value = "Duration"
            sheet.merge_cells(start_row=sr + 1, start_column=9, end_row=sr + 1, end_column=15)
            sheet.cell(row=sr + 1, column=9).value = "Losing Team"
            sheet.cell(row=sr + 1, column=9).alignment = Alignment(horizontal='center')

            sheet.cell(row=sr + 2, column=1).value = "Team Name"
            sheet.cell(row=sr + 2, column=2).value = "Team Score"
            sheet.cell(row=sr + 2, column=3).value = "Players"
            sheet.cell(row=sr + 2, column=4).value = "Kill"
            sheet.cell(row=sr + 2, column=5).value = "Death"
            sheet.cell(row=sr + 2, column=6).value = "Assist"
            sheet.cell(row=sr + 2, column=7).value = "Networth"
            sheet.cell(row=sr + 2, column=9).value = "Team Name"
            sheet.cell(row=sr + 2, column=10).value = "Team Score"
            sheet.cell(row=sr + 2, column=11).value = "Players"
            sheet.cell(row=sr + 2, column=12).value = "Kill"
            sheet.cell(row=sr + 2, column=13).value = "Death"
            sheet.cell(row=sr + 2, column=14).value = "Assist"
            sheet.cell(row=sr + 2, column=15).value = "Networth"

            # -------------------- value --------------------
            if game_info['Game Score']['victory'] == 0:
                pt1 = 0
                pt2 = 8
            else:
                pt1 = 8
                pt2 = 0

            sheet.merge_cells(start_row=sr + 3, start_column=1, end_row=sr + 7, end_column=1)
            sheet.cell(row=sr + 3, column=1).alignment = Alignment(horizontal='center', vertical='center')
            sheet.merge_cells(start_row=sr + 3, start_column=9, end_row=sr + 7, end_column=9)
            sheet.cell(row=sr + 3, column=9).alignment = Alignment(horizontal='center', vertical='center')
            sheet.cell(row=sr + 3, column=pt1 + 1).value = game_info['Game Score']['team1']
            sheet.cell(row=sr + 3, column=pt2 + 1).value = game_info['Game Score']['team2']

            sheet.merge_cells(start_row=sr + 3, start_column=2, end_row=sr + 7, end_column=2)
            sheet.cell(row=sr + 3, column=2).alignment = Alignment(horizontal='center', vertical='center')
            sheet.merge_cells(start_row=sr + 3, start_column=10, end_row=sr + 7, end_column=10)
            sheet.cell(row=sr + 3, column=10).alignment = Alignment(horizontal='center', vertical='center')
            sheet.cell(row=sr + 3, column=pt1 + 2).value = game_info['Game Score']['score1']
            sheet.cell(row=sr + 3, column=pt2 + 2).value = game_info['Game Score']['score2']

            sheet.merge_cells(start_row=sr + 2, start_column=8, end_row=sr + 7, end_column=8)
            sheet.cell(row=sr + 2, column=8).alignment = Alignment(horizontal='center', vertical='center')
            sheet.cell(row=sr + 2, column=8).value = game_info['Game Score']['duration']

            for i in range(5):
                sheet.cell(row=sr + 3 + i, column=pt1 + 3).value = game_info['Game Score']['players'][i]['name']
                sheet.cell(row=sr + 3 + i, column=pt1 + 4).value = game_info['Game Score']['players'][i]['kill']
                sheet.cell(row=sr + 3 + i, column=pt1 + 5).value = game_info['Game Score']['players'][i]['death']
                sheet.cell(row=sr + 3 + i, column=pt1 + 6).value = game_info['Game Score']['players'][i]['assist']
                sheet.cell(row=sr + 3 + i, column=pt1 + 7).value = game_info['Game Score']['players'][i]['money']
                sheet.cell(row=sr + 3 + i, column=pt2 + 3).value = game_info['Game Score']['players'][i + 5]['name']
                sheet.cell(row=sr + 3 + i, column=pt2 + 4).value = game_info['Game Score']['players'][i + 5]['kill']
                sheet.cell(row=sr + 3 + i, column=pt2 + 5).value = game_info['Game Score']['players'][i + 5]['death']
                sheet.cell(row=sr + 3 + i, column=pt2 + 6).value = game_info['Game Score']['players'][i + 5]['assist']
                sheet.cell(row=sr + 3 + i, column=pt2 + 7).value = game_info['Game Score']['players'][i + 5]['money']

        wb.save(excel_file)
        wb.close()
