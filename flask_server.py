from flask import Flask, request, jsonify
from flask_restful import Resource, Api
from main import ProcessImage
from datetime import datetime
import shutil
import os


class FaceApi(Resource):

    def __init__(self):
        self.store_data = {}

    def post(self):
        request_json = request.get_json()

        print("Received request!")
        req_output_mode = request_json['output_mode']
        req_img_list_data = request_json['images']

        folder_name = 'temp_{}'.format(datetime.now().microsecond)
        os.mkdir(folder_name)
        for i in range(len(req_img_list_data)):
            write_img_path = os.path.join(folder_name, req_img_list_data[i]["name"])
            class_main.class_func.img_decode_b64(req_img_list_data[i]["image_base64"], write_img_path)

        if req_output_mode == 'excel':
            class_main.process_folder(folder_name, output_json=False)
            result_status = "Success"
            result = class_main.class_func.img_encode_b64('result.xlsx')
        elif req_output_mode == 'json':
            result_status = "Success"
            result = class_main.process_folder(folder_name, output_json=True)
        else:
            result_status = "Invalid request"
            result = {}

        shutil.rmtree(folder_name)

        return jsonify(dict(result=result, status=result_status))


app = Flask(__name__)
api = Api(app)
api.add_resource(FaceApi, '/game_ocr/v1.0')


# ------- run main face engine for camera urls ---------
class_main = ProcessImage()


if __name__ == '__main__':
    app.run(host="0.0.0.0",
            port=int(os.environ.get("PORT", 3000)),
            debug=False,
            threaded=True)
