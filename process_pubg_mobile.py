from FuncMl import FuncMl
from tf_detector import TfDetector
from openpyxl.styles import Alignment
import numpy as np
import openpyxl
import cv2
import os


class ProcessPubgMobile:

    def __init__(self):
        self.class_detect = TfDetector('model/frozen_inference_graph.pb')
        self.class_func = FuncMl()

    def get_rank(self, img):
        img_rank = self.class_func.get_right_top_corner_image(img)
        detect_rect_list, detect_score_list, detect_class_list = self.class_detect.detect_from_images(img_rank)
        class_list = []
        pos_list = []
        for i in range(len(detect_rect_list)):
            if detect_score_list[i] > 0.3:
                pos_list.append(detect_rect_list[i][0])
                class_list.append(int(detect_class_list[i]))

        rank_list = []
        while len(class_list) > 0:
            ind = int(np.argmin(pos_list))
            rank_list.append(str(class_list[ind]).replace('10', '0'))
            pos_list.pop(ind)
            class_list.pop(ind)

        if rank_list[0] == '11':
            return ''.join(rank_list[1:])
        else:
            return '0'

    def get_info(self, img, ocr_json, debug=False):
        ret = {}
        # ------------------------ detect the region of keys ----------------------------
        pos_player, pos_kills, pos_damage, pos_survived, pos_revive, pos_rating, pos_pubg, pos_report = \
            None, None, None, None, None, None, None, None

        for i in range(1, len(ocr_json)):
            if ocr_json[i]['description'] == 'Player':
                pos_player = self.class_func.get_rect_ocr_data(ocr_json, i)

            if ocr_json[i]['description'] == 'Kills':
                pos_kills = self.class_func.get_rect_ocr_data(ocr_json, i)

            if ocr_json[i]['description'] == 'Damage':
                pos_damage = self.class_func.get_rect_ocr_data(ocr_json, i)

            if ocr_json[i]['description'] == 'Survived':
                pos_survived = self.class_func.get_rect_ocr_data(ocr_json, i)

            if ocr_json[i]['description'] == 'Revive':
                pos_revive = self.class_func.get_rect_ocr_data(ocr_json, i)

            if ocr_json[i]['description'] == 'Rating':
                pos_rating = self.class_func.get_rect_ocr_data(ocr_json, i)

            if ocr_json[i]['description'] == 'PUBG':
                pos_pubg = self.class_func.get_rect_ocr_data(ocr_json, i)

            if ocr_json[i]['description'] == 'Report':
                pos_report = self.class_func.get_rect_ocr_data(ocr_json, i)

        # -------------------------------- map name --------------------------------------
        text_lines = ocr_json[0]['description'].splitlines()
        ret['map'] = ''
        for i in range(len(text_lines)):
            if 'Classic' in text_lines[i] and 'Squad' in text_lines[i]:
                end_text = text_lines[i][text_lines[i].index('Squad') + 6:]
                ret['map'] = end_text.strip('-').strip()
                break

        # ------------------------------ extract rank ------------------------------------
        ret['rank'] = self.get_rank(img)

        # -------------------------- extract individual info -----------------------------
        x1, x2, x3, x4, y1, y2 = 0, 0, 0, 0, 0, 0
        if pos_pubg is not None:
            y2 = pos_pubg[1]
        elif pos_report is not None:
            y2 = pos_report[1] * 2 - pos_report[3]

        if pos_player:
            x1 = pos_player[0]
            y1 = pos_player[3]

        if pos_rating is not None:
            x2 = pos_rating[2]
        elif pos_revive is not None:
            x2 = pos_revive[2] * 2 - pos_revive[0]

        rect_players = []
        for i in range(4):
            rect_players.append([x1, int(y1 + i * (y2 - y1) / 4), x2, int(y1 + (i + 1) * (y2 - y1) / 4)])

        info_players = []
        for i in range(len(rect_players)):
            player_name = ''
            player_role = ''
            player_kills = '1'
            player_damage = ''
            player_survived = ''
            player_revive = ''
            player_rating = ''
            player_mvp = ''

            for j in range(1, len(ocr_json)):
                ocr_rect = self.class_func.get_rect_ocr_data(ocr_json, j)
                if self.class_func.check_contain_rects(rect_players[i], ocr_rect):
                    if ocr_rect[2] < pos_kills[0]:
                        player_name += ocr_json[j]['description'] + ' '
                    elif ocr_rect[2] < pos_damage[0] * 0.9 + pos_kills[0] * 0.1:
                        player_role += ocr_json[j]['description'] + ' '
                    elif ocr_rect[2] < pos_damage[2]:
                        player_kills = ocr_json[j]['description']
                    elif ocr_rect[2] < pos_survived[0]:
                        player_damage = ocr_json[j]['description']
                    elif ocr_rect[0] < pos_survived[2]:
                        player_survived += ocr_json[j]['description'] + ' '
                    elif ocr_rect[0] < pos_revive[0]:
                        player_revive = ocr_json[j]['description']
                    elif ocr_json[j]['description'] == 'MVP':
                        player_mvp = 'MVP'
                    else:
                        player_rating += ocr_json[j]['description'] + ' '

            info_players.append({'name': player_name.replace('[ ', '[').replace(' ]', ']').strip(),
                                 'role': player_role.strip(),
                                 'kills': player_kills.strip(),
                                 'damage': player_damage.strip(),
                                 'survived': player_survived.strip(),
                                 'revive': player_revive.strip().replace('O', '0'),
                                 'rating': player_rating.strip(),
                                 'mvp': player_mvp
                                 })

        ret['players'] = info_players

        # ---------------------------------- draw result ----------------------------------
        if debug:
            for i in range(len(rect_players)):
                cv2.rectangle(img, tuple(rect_players[i][:2]), tuple(rect_players[i][2:]), (255, 255, 0), 2)

            cv2.rectangle(img, tuple(pos_player[:2]), tuple(pos_player[2:]), (0, 255, 0), 2)
            cv2.rectangle(img, tuple(pos_kills[:2]), tuple(pos_kills[2:]), (0, 255, 0), 2)
            cv2.rectangle(img, tuple(pos_damage[:2]), tuple(pos_damage[2:]), (0, 255, 0), 2)
            cv2.rectangle(img, tuple(pos_survived[:2]), tuple(pos_survived[2:]), (0, 255, 0), 2)
            cv2.rectangle(img, tuple(pos_revive[:2]), tuple(pos_revive[2:]), (0, 255, 0), 2)
            cv2.rectangle(img, tuple(pos_rating[:2]), tuple(pos_rating[2:]), (0, 255, 0), 2)
            if pos_pubg is not None:
                cv2.rectangle(img, tuple(pos_pubg[:2]), tuple(pos_pubg[2:]), (0, 255, 0), 2)

            cv2.imshow('ret', img)
            cv2.waitKey()

        return ret

    def save_excel(self, excel_file, game_info_list, sheet_title):
        # ------------------ read excel file and add sheet --------------------
        if not os.path.isfile(excel_file):
            wb = openpyxl.Workbook()
            sheet_names = wb.sheetnames
            sheet = wb[sheet_names[0]]
            sheet.title = sheet_title
        else:
            wb = openpyxl.load_workbook(excel_file)
            sheet = wb.create_sheet(sheet_title)

        # ----------------------- write data to sheet ------------------------
        for game_ind in range(len(game_info_list)):
            game_info = game_info_list[game_ind]
            sr = game_ind * 7

            # ----------------- title --------------------
            sheet.cell(row=sr + 1, column=1).value = "Name"
            sheet.cell(row=sr + 1, column=2).value = "Role"
            sheet.cell(row=sr + 1, column=3).value = "Kills"
            sheet.cell(row=sr + 1, column=4).value = "Damage"
            sheet.cell(row=sr + 1, column=5).value = "Survived"
            sheet.cell(row=sr + 1, column=6).value = "Rank"
            sheet.cell(row=sr + 1, column=7).value = "Rating"
            sheet.cell(row=sr + 1, column=8).value = "MVP"
            sheet.cell(row=sr + 1, column=9).value = "Map"
            sheet.cell(row=sr + 1, column=10).value = "TEAM KD"
            sheet.cell(row=sr + 1, column=11).value = "TEAM DMG"
            sheet.cell(row=sr + 1, column=12).value = "TEAM Survival"
            sheet.cell(row=sr + 1, column=13).value = "Team Rating"

            for i in range(13):
                sheet.cell(row=sr + 1, column=i + 1).alignment = Alignment(horizontal='center')

            # -------------------- player value --------------------
            sum_kills, sum_damage, sum_sur, sum_rating = 0, 0, 0, 0
            for i in range(len(game_info['Game Score']['players'])):
                val_kills = self.class_func.get_float(game_info['Game Score']['players'][i]['kills'])
                val_damage = self.class_func.get_float(game_info['Game Score']['players'][i]['damage'])
                val_sur = self.class_func.get_float(game_info['Game Score']['players'][i]['survived'])
                val_rating = self.class_func.get_float(game_info['Game Score']['players'][i]['rating'])
                sum_kills += val_kills
                sum_damage += val_damage
                sum_sur += val_sur
                sum_rating += val_rating
                sheet.cell(row=sr + 2 + i, column=1).value = game_info['Game Score']['players'][i]['name']
                sheet.cell(row=sr + 2 + i, column=2).value = game_info['Game Score']['players'][i]['role']
                sheet.cell(row=sr + 2 + i, column=3).value = val_kills
                sheet.cell(row=sr + 2 + i, column=4).value = val_damage
                sheet.cell(row=sr + 2 + i, column=5).value = val_sur
                sheet.cell(row=sr + 2 + i, column=7).value = val_rating
                sheet.cell(row=sr + 2 + i, column=8).value = game_info['Game Score']['players'][i]['mvp']

            # ---------------------- team value ----------------------
            for i in [6, 9, 10, 11, 12, 13]:
                sheet.merge_cells(start_row=sr + 2, start_column=i,
                                  end_row=sr + len(game_info['Game Score']['players']) + 1, end_column=i)
                sheet.cell(row=sr + 2, column=i).alignment = Alignment(vertical='center')

            sheet.cell(row=sr + 2, column=6).value = game_info['Game Score']['rank']
            sheet.cell(row=sr + 2, column=9).value = game_info['Game Score']['map']
            sheet.cell(row=sr + 2, column=10).value = sum_kills / len(game_info['Game Score']['players'])
            sheet.cell(row=sr + 2, column=11).value = sum_damage / len(game_info['Game Score']['players'])
            sheet.cell(row=sr + 2, column=12).value = sum_sur / len(game_info['Game Score']['players'])
            sheet.cell(row=sr + 2, column=13).value = sum_rating / len(game_info['Game Score']['players'])

        wb.save(excel_file)
        wb.close()
