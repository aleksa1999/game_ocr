from FuncMl import FuncMl
from openpyxl.styles import Alignment
import openpyxl
import numpy as np
import cv2
import os


OBJ_LIST = ['GOLD', 'SILVER', 'BRONZE']


class ProcessMobileLegend:

    def __init__(self):
        self.class_func = FuncMl()
        self.templates = self.load_templates()

    def load_templates(self):
        templates = {}
        _, file_list, join_list = self.class_func.get_file_list('template')

        for file_ind in range(len(file_list)):
            template_key = file_list[file_ind].split('_')[0]
            if template_key in OBJ_LIST:
                img_key = cv2.imread(join_list[file_ind], 0)

                if template_key in templates:
                    templates[template_key].append(img_key)
                else:
                    templates[template_key] = [img_key]

        return templates

    def detect_medal(self, img_rgb):
        img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)

        detect_result = []

        for key in self.templates:
            for template in self.templates[key]:
                w, h = template.shape[::-1]

                res = cv2.matchTemplate(img_gray, template, cv2.TM_CCOEFF_NORMED)
                threshold = 0.95
                loc = np.where(res >= threshold)

                for pt in zip(*loc[::-1]):
                    cur_rect = [pt[0], pt[1], pt[0] + w, pt[1] + h]

                    f_same = False
                    for j in range(len(detect_result)):
                        detect = detect_result[j]
                        pre_rect = detect['rect']

                        if self.class_func.check_same_rect(pre_rect, cur_rect):
                            f_same = True
                            if self.class_func.area(pre_rect) < self.class_func.area(cur_rect):
                                detect_result[j] = {'name': key, 'rect': cur_rect}
                            break

                    if not f_same:
                        detect_result.append({'name': key, 'rect': cur_rect})

        return detect_result

    def get_info(self, img, ocr_json, debug=False):
        # ------------------------ detect the region of keys ----------------------------
        pos_duration, pos_victory, pos_defeat, pos_data, pos_return = None, None, None, None, None
        for i in range(1, len(ocr_json)):
            if ocr_json[i]['description'] == 'Duration':
                pos_duration = self.class_func.get_rect_ocr_data(ocr_json, i)

            if ocr_json[i]['description'] == 'VICTORY':
                pos_victory = self.class_func.get_rect_ocr_data(ocr_json, i)

            if ocr_json[i]['description'] == 'DEFEAT':
                pos_defeat = self.class_func.get_rect_ocr_data(ocr_json, i)

            if ocr_json[i]['description'] == 'Data':
                pos_data = self.class_func.get_rect_ocr_data(ocr_json, i)

            if ocr_json[i]['description'] == 'Return':
                pos_return = self.class_func.get_rect_ocr_data(ocr_json, i)

        # ------------------------- template match for medal -----------------------------
        medal_list = self.detect_medal(img)

        # -------------------------- extract individual info -----------------------------
        if pos_victory is not None:
            cx = int((pos_victory[0] + pos_victory[2]) / 2)
            cy1 = pos_victory[1]
            cy2 = pos_victory[3]
        else:
            cx = int((pos_defeat[0] + pos_defeat[2]) / 2)
            cy1 = pos_defeat[1]
            cy2 = pos_defeat[3]

        if pos_data is not None and pos_return is not None:
            x1 = int(pos_data[2] * 0.86 + 0.14 * cx)
            x2 = int(pos_data[2] * 0.22 + 0.78 * cx)
            y1 = int(cy2 * 0.85 + pos_data[1] * 0.15)
            y2 = int(cy2 * 0.1 + pos_data[1] * 0.9)
            x3 = int(pos_return[0] * 0.22 + 0.78 * cx)
            x4 = int(pos_return[0] * 0.85 + 0.15 * cx)
        else:
            x1, x2, x3, x4, y1, y2 = 0, 0, 0, 0, 0, 0

        rect_players = []
        rect_scores = []
        for i in range(5):
            rect_players.append([x1, int(y1 + i * (y2 - y1) / 5), x2, int(y1 + (i + 1) * (y2 - y1) / 5)])
            rect_scores.append([x2 + 5, int(y1 + i * (y2 - y1) / 5), cx - 5, int(y1 + (i + 1) * (y2 - y1) / 5)])

        for i in range(5):
            rect_players.append([x3, int(y1 + i * (y2 - y1) / 5), x4, int(y1 + (i + 1) * (y2 - y1) / 5)])
            rect_scores.append([cx + 5, int(y1 + i * (y2 - y1) / 5), x3 - 5, int(y1 + (i + 1) * (y2 - y1) / 5)])

        info_players = []
        for i in range(10):
            word_list_player = []
            left_list_player = []
            for j in range(1, len(ocr_json)):
                ocr_rect = self.class_func.get_rect_ocr_data(ocr_json, j)
                if self.class_func.check_contain_rects(rect_players[i], ocr_rect):
                    ocr_text = ocr_json[j]['description']
                    width_rate = (ocr_rect[2] - ocr_rect[0]) / (ocr_rect[3] - ocr_rect[1]) / len(ocr_text)
                    insert_pos = len(left_list_player)
                    for k in range(len(left_list_player)):
                        if ocr_rect[0] < left_list_player[k]:
                            insert_pos = k
                            break

                    if ocr_text.isdigit() and len(ocr_text) >= 2 and width_rate > 0.65:
                        if len(ocr_text) == 3 and ocr_text[1] == '0':
                            word_list_player.insert(insert_pos, ocr_text[:2])
                            word_list_player.insert(insert_pos + 1, ocr_text[2])
                        else:
                            word_list_player.insert(insert_pos, ocr_text[0])
                            word_list_player.insert(insert_pos + 1, ocr_text[1:])
                        left_list_player.insert(insert_pos, ocr_rect[0])
                        left_list_player.insert(insert_pos + 1, ocr_rect[0] + 10)
                    else:
                        word_list_player.insert(insert_pos, ocr_text)
                        left_list_player.insert(insert_pos, ocr_rect[0])

            word_list_score = []
            for j in range(1, len(ocr_json)):
                ocr_rect = self.class_func.get_rect_ocr_data(ocr_json, j)
                if self.class_func.check_contain_rects(rect_scores[i], ocr_rect):
                    word_list_score.append(ocr_json[j]['description'])

            player_name = ''
            player_money = ''
            player_kill = ''
            player_death = ''
            player_assist = ''
            player_score = ''
            player_medal = ''

            if len(word_list_player) >= 4:
                if i < 5 and ''.join(word_list_player[-4:]).isdigit():
                    player_name = ' '.join(word_list_player[:-4])
                    player_kill = word_list_player[-4]
                    player_death = word_list_player[-3]
                    player_assist = word_list_player[-2]
                    player_money = word_list_player[-1]
                elif i >= 5:
                    if ''.join(word_list_player[:4]).isdigit():
                        player_money = word_list_player[0]
                        player_kill = word_list_player[1]
                        player_death = word_list_player[2]
                        player_assist = word_list_player[3]
                        player_name = ' '.join(word_list_player[4:])
                    elif ''.join(word_list_player[:3]).replace('.', '').isdigit() and '.' in word_list_player[1]:
                        player_money = word_list_player[0]
                        player_kill = word_list_player[1][:word_list_player[1].index('.')]
                        player_death = word_list_player[1][word_list_player[1].index('.') + 1:]
                        player_assist = word_list_player[2]
                        player_name = ' '.join(word_list_player[3:])

            if len(word_list_score) > 0 and word_list_score[-1].replace('.', '').replace(',', '').isdigit():
                player_score = word_list_score[-1].replace(',', '')
                if '.' not in player_score:
                    player_score = player_score[:-1] + '.' + player_score[-1]

            if len(word_list_score) > 0 and word_list_score[0] == 'MVP':
                player_medal = 'MVP'
            else:
                for j in range(len(medal_list)):
                    if self.class_func.check_contain_rects(rect_scores[i], medal_list[j]['rect']):
                        player_medal = medal_list[j]['name']
                        break

            info_players.append({'name': player_name,
                                 'money': player_money,
                                 'kill': player_kill,
                                 'death': player_death,
                                 'assist': player_assist,
                                 'score': player_score,
                                 'medal': player_medal})

        ret = {'players': info_players}

        # --------------------------- extract match info ---------------------------------
        if pos_victory is not None:
            ret['victory'] = 0
        elif pos_defeat is not None:
            ret['victory'] = 1
        else:
            ret['victory'] = -1

        rect_score1 = [int((rect_players[0][0] + rect_players[0][2]) / 2),
                       int(cy1 - (cy2 - cy1) / 2),
                       int(rect_players[0][0] * 0.2 + rect_players[0][2] * 0.8),
                       int(cy2 + (cy2 - cy1) / 2)]
        rect_score2 = [int(rect_players[5][0] * 0.8 + rect_players[5][2] * 0.2),
                       int(cy1 - (cy2 - cy1) / 2),
                       int((rect_players[5][0] + rect_players[5][2]) / 2),
                       int(cy2 + (cy2 - cy1) / 2)]
        if pos_duration is None:
            rect_time = [0, 0, 0, 0]
        else:
            rect_time = [int(pos_duration[2]),
                         int(pos_duration[1] * 1.5 - pos_duration[3] * 0.5),
                         int(pos_duration[2] * 2 - pos_duration[0]),
                         int(pos_duration[3] * 1.5 - pos_duration[1] * 0.5)]

        ret["duration"] = ''
        ret["team1"] = ''
        ret['team2'] = ''
        for i in range(1, len(ocr_json)):
            ocr_rect = self.class_func.get_rect_ocr_data(ocr_json, i)
            if self.class_func.check_contain_rects(rect_score1, ocr_rect) and ocr_json[i]['description'].isdigit():
                ret["score1"] = ocr_json[i]['description']
            elif self.class_func.check_contain_rects(rect_score2, ocr_rect) and ocr_json[i]['description'].isdigit():
                ret["score2"] = ocr_json[i]['description']
            elif self.class_func.check_contain_rects(rect_time, ocr_rect):
                if ocr_json[i]['description'].replace(':', '').isdigit() and ':' in ocr_json[i]['description']:
                    ret["duration"] = ocr_json[i]['description']
                elif ocr_json[i]['description'].isdigit() or ocr_json[i]['description'] == ':':
                    ret["duration"] += ocr_json[i]['description']

        # ---------------------------------- draw result ----------------------------------
        if debug:
            for i in range(len(rect_players)):
                cv2.rectangle(img, tuple(rect_players[i][:2]), tuple(rect_players[i][2:]), (0, 255, 0), 2)
                cv2.rectangle(img, tuple(rect_scores[i][:2]), tuple(rect_scores[i][2:]), (0, 255, 0), 2)

            cv2.rectangle(img, tuple(rect_score1[:2]), tuple(rect_score1[2:]), (0, 255, 0), 2)
            cv2.rectangle(img, tuple(rect_score2[:2]), tuple(rect_score2[2:]), (0, 255, 0), 2)
            cv2.rectangle(img, tuple(rect_time[:2]), tuple(rect_time[2:]), (0, 255, 0), 2)

            for i in range(len(medal_list)):
                cv2.rectangle(img, tuple(medal_list[i]['rect'][:2]), tuple(medal_list[i]['rect'][2:]), (0, 255, 0), 2)
                cv2.putText(img, medal_list[i]['name'], tuple(medal_list[i]['rect'][:2]), cv2.FONT_HERSHEY_SIMPLEX, 1,
                            (0, 0, 255), 2)

            # cv2.imshow('ret', img)
            # cv2.waitKey()

        return ret

    @staticmethod
    def save_excel(excel_file, game_info_list, sheet_title):
        # ------------------ read excel file and add sheet --------------------
        if not os.path.isfile(excel_file):
            wb = openpyxl.Workbook()
            sheet_names = wb.sheetnames
            sheet = wb[sheet_names[0]]
            sheet.title = sheet_title
        else:
            wb = openpyxl.load_workbook(excel_file)
            sheet = wb.create_sheet(sheet_title)

        # ----------------------- write data to sheet ------------------------
        for game_ind in range(len(game_info_list)):
            game_info = game_info_list[game_ind]
            sr = game_ind * 10

            # ----------------- title --------------------
            sheet.merge_cells(start_row=sr + 1, start_column=1, end_row=sr + 1, end_column=9)
            sheet.cell(row=sr + 1, column=1).value = "Winning Team"
            sheet.cell(row=sr + 1, column=1).alignment = Alignment(horizontal='center')
            sheet.cell(row=sr + 1, column=10).value = "Duration"
            sheet.merge_cells(start_row=sr + 1, start_column=11, end_row=sr + 1, end_column=19)
            sheet.cell(row=sr + 1, column=11).value = "Losing Team"
            sheet.cell(row=sr + 1, column=11).alignment = Alignment(horizontal='center')

            sheet.cell(row=sr + 2, column=1).value = "Team Name"
            sheet.cell(row=sr + 2, column=2).value = "Team Score"
            sheet.cell(row=sr + 2, column=3).value = "Players"
            sheet.cell(row=sr + 2, column=4).value = "Kill"
            sheet.cell(row=sr + 2, column=5).value = "Death"
            sheet.cell(row=sr + 2, column=6).value = "Assist"
            sheet.cell(row=sr + 2, column=7).value = "Gold"
            sheet.cell(row=sr + 2, column=8).value = "Score"
            sheet.cell(row=sr + 2, column=9).value = "Medal"
            sheet.cell(row=sr + 2, column=11).value = "Team Name"
            sheet.cell(row=sr + 2, column=12).value = "Team Score"
            sheet.cell(row=sr + 2, column=13).value = "Players"
            sheet.cell(row=sr + 2, column=14).value = "Kill"
            sheet.cell(row=sr + 2, column=15).value = "Death"
            sheet.cell(row=sr + 2, column=16).value = "Assist"
            sheet.cell(row=sr + 2, column=17).value = "Gold"
            sheet.cell(row=sr + 2, column=18).value = "Score"
            sheet.cell(row=sr + 2, column=19).value = "Medal"

            # -------------------- value --------------------
            if game_info['Game Score']['victory'] == 0:
                pt1 = 0
                pt2 = 10
            else:
                pt1 = 10
                pt2 = 0

            sheet.merge_cells(start_row=sr + 3, start_column=1, end_row=sr + 7, end_column=1)
            sheet.cell(row=sr + 3, column=1).alignment = Alignment(horizontal='center', vertical='center')
            sheet.merge_cells(start_row=sr + 3, start_column=11, end_row=sr + 7, end_column=11)
            sheet.cell(row=sr + 3, column=11).alignment = Alignment(horizontal='center', vertical='center')
            sheet.cell(row=sr + 3, column=pt1 + 1).value = game_info['Game Score']['team1']
            sheet.cell(row=sr + 3, column=pt2 + 1).value = game_info['Game Score']['team2']

            sheet.merge_cells(start_row=sr + 3, start_column=2, end_row=sr + 7, end_column=2)
            sheet.cell(row=sr + 3, column=2).alignment = Alignment(horizontal='center', vertical='center')
            sheet.merge_cells(start_row=sr + 3, start_column=12, end_row=sr + 7, end_column=12)
            sheet.cell(row=sr + 3, column=12).alignment = Alignment(horizontal='center', vertical='center')
            sheet.cell(row=sr + 3, column=pt1 + 2).value = game_info['Game Score']['score1']
            sheet.cell(row=sr + 3, column=pt2 + 2).value = game_info['Game Score']['score2']

            sheet.merge_cells(start_row=sr + 2, start_column=10, end_row=sr + 7, end_column=10)
            sheet.cell(row=sr + 2, column=10).alignment = Alignment(horizontal='center', vertical='center')
            sheet.cell(row=sr + 2, column=10).value = game_info['Game Score']['duration']

            for i in range(5):
                sheet.cell(row=sr + 3 + i, column=pt1 + 3).value = game_info['Game Score']['players'][i]['name']
                sheet.cell(row=sr + 3 + i, column=pt1 + 4).value = game_info['Game Score']['players'][i]['kill']
                sheet.cell(row=sr + 3 + i, column=pt1 + 5).value = game_info['Game Score']['players'][i]['death']
                sheet.cell(row=sr + 3 + i, column=pt1 + 6).value = game_info['Game Score']['players'][i]['assist']
                sheet.cell(row=sr + 3 + i, column=pt1 + 7).value = game_info['Game Score']['players'][i]['money']
                sheet.cell(row=sr + 3 + i, column=pt1 + 8).value = game_info['Game Score']['players'][i]['score']
                sheet.cell(row=sr + 3 + i, column=pt1 + 9).value = game_info['Game Score']['players'][i]['medal']
                sheet.cell(row=sr + 3 + i, column=pt2 + 3).value = game_info['Game Score']['players'][i + 5]['name']
                sheet.cell(row=sr + 3 + i, column=pt2 + 4).value = game_info['Game Score']['players'][i + 5]['kill']
                sheet.cell(row=sr + 3 + i, column=pt2 + 5).value = game_info['Game Score']['players'][i + 5]['death']
                sheet.cell(row=sr + 3 + i, column=pt2 + 6).value = game_info['Game Score']['players'][i + 5]['assist']
                sheet.cell(row=sr + 3 + i, column=pt2 + 7).value = game_info['Game Score']['players'][i + 5]['money']
                sheet.cell(row=sr + 3 + i, column=pt2 + 8).value = game_info['Game Score']['players'][i + 5]['score']
                sheet.cell(row=sr + 3 + i, column=pt2 + 9).value = game_info['Game Score']['players'][i + 5]['medal']

        wb.save(excel_file)
        wb.close()
