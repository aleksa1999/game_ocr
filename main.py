from process_pubg import ProcessPUBG
from process_dota import ProcessDOTA
from process_ml import ProcessMobileLegend
from process_pubg_mobile import ProcessPubgMobile
from FuncMl import FuncMl
import json
import cv2
import sys
import os


class ProcessImage:

    def __init__(self):
        self.class_func = FuncMl()
        self.class_pubg = ProcessPUBG()
        self.class_dota = ProcessDOTA()
        self.class_legend = ProcessMobileLegend()
        self.class_pubg_mobile = ProcessPubgMobile()

    def process_file(self, file_image, debug=False):
        # ----------------- read image and get OCR info -----------------------
        img = cv2.imread(file_image)
        ret_json = self.class_func.get_json_google_from_jpg(file_image)

        # -------------------- detect the game type ---------------------------
        ocr_text = ret_json[0]['description']
        if 'PUBG' in ocr_text and 'Damage' in ocr_text and ('Weapon' in ocr_text or 'Report' in ocr_text):
            game_type = 'PUBG_MOBILE'
            game_score = self.class_pubg_mobile.get_info(img, ret_json, debug=debug)
        elif 'PUBG' in ocr_text:
            game_type = 'PUBG'
            game_score = self.class_pubg.get_info(img, ret_json, debug=debug)
        elif all(s in ocr_text for s in ['HEROES', 'STORE', 'WATCH', 'LEARN']):
            game_type = 'DOTA2'
            game_score = self.class_dota.get_info(img, ret_json, debug=debug)
        elif all(s in ocr_text for s in ['Data', 'Return']) and \
                any(s in ocr_text for s in ['DEFEAT', 'VICTORY']):
            game_type = 'MOBILE_LEGEND'
            game_score = self.class_legend.get_info(img, ret_json, debug=debug)
        else:
            game_type = 'Unknown'
            game_score = []

        game_info = {'Game Type': game_type, 'Game Score': game_score}

        # ------------------- save and show ocr result ------------------------
        if debug:
            print(json.dumps(game_info, indent=4))
            img_ocr = img.copy()
            for i in range(1, len(ret_json)):
                pos = self.class_func.get_rect_ocr_data(ret_json, i)
                cv2.rectangle(img_ocr, (pos[0], pos[1]), (pos[2], pos[3]), (255, 0, 0), 1)
            cv2.imwrite('a_ret.jpg', img_ocr)
            cv2.imshow('ocr', img_ocr)
            cv2.waitKey(0)

        return game_info

    def process_folder(self, source, output_json=False, excel_file='result.xlsx'):
        print(f'Processing folder {source} ...')
        _, _, join_list = self.class_func.get_file_list(source)

        # ------------------- extract the game info from the folder --------------------
        game_type = ''
        game_score = []

        for i in range(len(join_list)):
            if join_list[i][-4:].lower() not in ['.jpg', '.png', '.bmp', 'jpeg']:
                continue

            print(f'\tProcessing file {join_list[i]}')
            game_info = self.process_file(join_list[i])
            game_type = game_info['Game Type']

            if game_type == 'PUBG':
                for j in range(len(game_info['Game Score'])):
                    f_dup = False
                    for k in range(len(game_score)):
                        if game_info['Game Score'][j]['placement'] == game_score[k]['placement']:
                            f_dup = True
                            break

                    if not f_dup:
                        game_score.append(game_info['Game Score'][j])

            elif game_type in ['DOTA2', 'MOBILE_LEGEND', 'PUBG_MOBILE']:
                game_score.append(game_info)

            # break

        # ------------------------- output as json format --------------------------------
        if output_json:
            return game_score

        # ----------------- read the previous excel and append game data -----------------
        print("Write the result to excel file")
        if source[-1] == '/':
            sheet_name = os.path.basename(os.path.dirname(source))
        else:
            sheet_name = os.path.basename(source)

        if game_type == 'PUBG':
            # Add team name manually
            for i in range(len(game_score)):
                game_score[i]['team'] = 'Team_{}'.format(i + 1)

            game_data = self.class_pubg.read_excel(excel_file)
            game_data.append(game_score)

            excel_data = self.class_pubg.create_excel_data(game_data)
            self.class_pubg.save_excel(excel_file, excel_data, len(game_data))

        elif game_type == 'DOTA2':
            self.class_dota.save_excel(excel_file, game_score, sheet_name)

        elif game_type == 'MOBILE_LEGEND':
            self.class_legend.save_excel(excel_file, game_score, sheet_name)

        elif game_type == 'PUBG_MOBILE':
            self.class_pubg_mobile.save_excel(excel_file, game_score, sheet_name)

        else:
            pass

        return None


if __name__ == '__main__':
    if len(sys.argv) == 1:
        # source_name = ['images/Match1', 'images/Match2', 'images/Match3']
        source_name = ['../images/images_pubg3']
        # source_name = ['image_dota1', 'image_dota2']
    else:
        source_name = sys.argv[1:]

    class_process = ProcessImage()

    for folder_ind in range(len(source_name)):
        class_process.process_folder(source_name[folder_ind])

    # class_process.process_file('../images/images_pubg_mobile/1.jpg', debug=True)
