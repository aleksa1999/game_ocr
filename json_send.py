from FuncMl import FuncMl
import requests
import json
import sys


class_func = FuncMl()


def make_request_json_add(folder, output_mode):
    _, file_list, join_list = class_func.get_file_list(folder)
    req_json = {"images": [], "output_mode": output_mode}
    for i in range(len(file_list)):
        req_json["images"].append({'name': file_list[i], 'image_base64': class_func.img_encode_b64(join_list[i])})

    return req_json


def send_request(server, req_json):
    response = requests.post(url=server, json=req_json)
    print("Server responded with %s" % response.status_code)

    response_json = response.json()
    return response_json


if __name__ == '__main__':
    if len(sys.argv) == 2:
        folder_name = sys.argv[1]
    else:
        folder_name = '../images/images_pubg_mobile'

    url_server = 'http://localhost:3000/game_ocr/v1.0'
    # url_server = 'http://143.198.231.145:3000/card_service/v1.0'

    # json_request = make_request_json_add(folder=folder_name, output_mode="excel")
    json_request = make_request_json_add(folder=folder_name, output_mode="json")
    ret_response = send_request(url_server, json_request)

    print(json.dumps(ret_response, indent=4))
    # class_func.img_decode_b64(ret_response['result'], 'result_api.xlsx')
