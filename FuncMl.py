from datetime import datetime
import base64
import json
import os


class FuncMl:
    def __init__(self):
        """
            Constructor for the class
        """
        self.my_dir = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
        self.google_key = self.load_text(os.path.join(self.my_dir, 'config', 'vision_key.txt'))

    @staticmethod
    def load_text(filename):
        if os.path.isfile(filename):
            file1 = open(filename, 'r')
            text = file1.read()
            file1.close()
        else:
            text = ''

        return text

    @staticmethod
    def save_text(filename, text):
        file1 = open(filename, 'w')
        file1.write(text)
        file1.close()

    @staticmethod
    def save_text_binary(filename, text):
        file1 = open(filename, 'wb')
        file1.write(text)
        file1.close()

    @staticmethod
    def get_file_list(root_dir):
        """
            get all files in root_dir directory
        """
        path_list = []
        file_list = []
        join_list = []
        for path, _, files in os.walk(root_dir):
            for name in files:
                path_list.append(path)
                file_list.append(name)
                join_list.append(os.path.join(path, name))

        return path_list, file_list, join_list

    @staticmethod
    def rm_file(filename):
        if os.path.isfile(filename):
            os.remove(filename)

    @staticmethod
    def get_field_int(dict_data, field):
        if field in dict_data:
            return dict_data[field]
        else:
            return 0

    def get_rect_ocr_data(self, ocr_data, ind):
        p = ocr_data[ind]['boundingPoly']['vertices']
        rect = [self.get_field_int(p[0], 'x'),
                self.get_field_int(p[0], 'y'),
                self.get_field_int(p[1], 'x'),
                self.get_field_int(p[2], 'y')]

        return rect

    @staticmethod
    def __make_request_json(img_file, output_filename, detection_type='text'):

        # Read the image and convert to json
        with open(img_file, 'rb') as image_file:
            content_json_obj = {'content': base64.b64encode(image_file.read()).decode('UTF-8')}
            # content_json_obj = {'content': base64.b64encode(image_file.read())}

        if detection_type == 'text':
            feature_json_arr = [{'type': 'TEXT_DETECTION'}, {'type': 'DOCUMENT_TEXT_DETECTION'}]
        elif detection_type == 'logo':
            feature_json_arr = [{'type': 'LOGO_DETECTION'}]
        else:
            feature_json_arr = [{'type': 'TEXT_DETECTION'}, {'type': 'DOCUMENT_TEXT_DETECTION'}]

        request_list = {'features': feature_json_arr, 'image': content_json_obj}

        # Write the object to a file, as json
        with open(output_filename, 'w') as output_json:
            json.dump({'requests': [request_list]}, output_json)

    def __get_text_info(self, json_file, detection_type='text'):

        data = open(json_file, 'rb').read()
        try:
            import requests

            response = requests.post(
                url='https://vision.googleapis.com/v1/images:annotate?key=' + self.google_key,
                data=data,
                headers={'Content-Type': 'application/json'})

            ret_json = json.loads(response.text)
            ret_val = ret_json['responses'][0]

            if detection_type == 'text' and 'textAnnotations' in ret_val:
                return ret_val['textAnnotations']
            elif detection_type == 'logo' and 'logoAnnotations' in ret_val:
                return ret_val['logoAnnotations']
            else:
                return None

        except Exception as e:
            return None

    def get_json_google_from_jpg(self, img_file, detection_type='text'):

        temp_json = "temp" + str(datetime.now().microsecond) + '.json'

        # --------------------- Image crop and rescaling, then ocr ------------------
        if img_file is None:
            ret_json = None
        else:
            self.__make_request_json(img_file, temp_json, detection_type)
            ret_json = self.__get_text_info(temp_json, detection_type)
            # ret_json = self.__get_google_request(temp_json, detection_type)

        # --------------------- delete temporary files -------------------------------
        self.rm_file(temp_json)

        if ret_json is not None and detection_type == 'text':
            self.save_text_binary('a_ocr.txt', ret_json[0]['description'].encode('utf8'))

        return ret_json

    @staticmethod
    def check_contain_rects(big_rect, small_rect):
        if big_rect[0] <= small_rect[0] < small_rect[2] <= big_rect[2] and \
                big_rect[1] <= small_rect[1] < small_rect[3] <= big_rect[3]:
            return True
        else:
            return False

    @staticmethod
    def check_same_rect(rect1, rect2):
        w = max(rect1[2] - rect1[0], rect2[2] - rect2[0])
        h = max(rect1[3] - rect1[1], rect2[3] - rect2[1])

        if all([2 * abs(rect1[0] - rect2[0]) < w,
                2 * abs(rect1[2] - rect2[2]) < w,
                2 * abs(rect1[1] - rect2[1]) < h,
                2 * abs(rect1[3] - rect2[3]) < h]):
            return True

        else:
            return False

    @staticmethod
    def area(rect):
        return (rect[2] - rect[0]) * (rect[3] - rect[1])

    @staticmethod
    def img_encode_b64(img_file):
        file_data = open(img_file, 'rb')
        return base64.b64encode(file_data.read()).decode('UTF-8')

    @staticmethod
    def img_decode_b64(img_date, img_name):
        img_out = open(img_name, 'wb')
        img_out.write(base64.b64decode(img_date))
        img_out.close()

    @staticmethod
    def get_right_top_corner_image(img):
        img_h, img_w = img.shape[:2]
        return img[:int(img_h / 4), int(img_w * 3 / 4):]

    @staticmethod
    def get_float(text):
        text = text.rstrip('m')
        try:
            val = float(text)
            return val
        except ValueError:
            return 0
