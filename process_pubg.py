from FuncMl import FuncMl
from fuzzywuzzy import fuzz
import numpy as np
from openpyxl.styles import Alignment
import openpyxl
import cv2
import os


class ProcessPUBG:
    def __init__(self):
        self.class_func = FuncMl()

    @staticmethod
    def read_excel(excel_file):
        if not os.path.isfile(excel_file):
            return []

        wb = openpyxl.load_workbook(excel_file)
        sheet_names = wb.sheetnames
        sheet = wb[sheet_names[0]]

        # ---------------------- detect the map and team counts ----------------------
        map_cnt = 1
        while sheet.cell(row=1, column=map_cnt * 6 + 9).value is not None:
            map_cnt += 1

        team_cnt = 1
        while sheet.cell(row=3 + team_cnt * 4, column=1).value is not None:
            team_cnt += 1

        # ------------------------ load the game info by map -------------------------
        game_info = []
        for map_ind in range(map_cnt):
            map_info = []
            for team_ind in range(team_cnt):
                if sheet.cell(row=3 + team_ind * 4, column=map_ind * 6 + 4).value is None:
                    continue

                team_info = {
                    'team': sheet.cell(row=3 + team_ind * 4, column=1).value,
                    'total_kills': sheet.cell(row=3 + team_ind * 4, column=map_ind * 6 + 4).value,
                    'placement': sheet.cell(row=3 + team_ind * 4, column=map_ind * 6 + 6).value,
                    'total_points': sheet.cell(row=3 + team_ind * 4, column=map_ind * 6 + 8).value,
                    'kills': [],
                    'players': []
                }

                for i in range(4):
                    if sheet.cell(row=3 + team_ind * 4 + i, column=2).value is not None:
                        team_info['players'].append(sheet.cell(row=3 + team_ind * 4 + i, column=2).value)
                        team_info['kills'].append(sheet.cell(row=3 + team_ind * 4 + i, column=map_ind * 6 + 3).value)

                map_info.append(team_info)

            game_info.append(map_info)

        return game_info

    def __crop_ocr__(self, ocr_json, region):
        ret = []
        for i in range(1, len(ocr_json)):
            rect = self.class_func.get_rect_ocr_data(ocr_json, i)
            rect_x, rect_y = int((rect[0] + rect[2]) / 2), int((rect[1] + rect[3]) / 2)
            if region[0] < rect_x < region[2] and region[1] < rect_y < region[3]:
                new_rect = [rect[0] - region[0], rect[1] - region[1], rect[2] - region[0], rect[3] - region[1]]
                ret.append([ocr_json[i]['description'], new_rect])

        return ret

    @staticmethod
    def calc_place_score(rank):
        if rank == 1:
            score = 15
        elif rank <= 7:
            score = 16 - rank * 2
        elif rank <= 12:
            score = 1
        else:
            score = 0

        return score

    def get_info(self, img, ocr_json, debug=False):
        # ------------------- Binary threshold and erode ------------------------
        # img_h, img_w = img.shape[:2]
        img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        ret, img_thresh = cv2.threshold(img_gray, 3, 255, cv2.THRESH_BINARY)
        kernel = np.ones((5, 5), np.uint8)
        img_erode = cv2.erode(img_thresh, kernel, iterations=1)

        # ---------------------- detect the score block -------------------------
        contours, hierarchy = cv2.findContours(img_erode, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        block_list = []
        for i in range(len(contours)):
            x, y, w, h = cv2.boundingRect(contours[i])
            if w > 400 and h > 60:
                score = x * 10 + y
                f_insert = False
                for j in range(len(block_list)):
                    if score < block_list[j][0] * 10 + block_list[j][1]:
                        block_list.insert(j, [x, y, x + w, y + h])
                        f_insert = True
                        break

                if not f_insert:
                    block_list.append([x, y, x + w, y + h])

        # ------------------------ extract the info ------------------------------
        ret_info = []
        for i in range(len(block_list)):
            block_words = self.__crop_ocr__(ocr_json, block_list[i])

            # === player names ===
            list_players = []
            list_index = []
            for j in range(len(block_words)):
                [word, word_pos] = block_words[j]
                if 50 < word_pos[0] < 350:
                    f_find = False
                    for k in range(len(list_index)):
                        if abs(list_index[k] - word_pos[1]) < 10:
                            list_players[k] += ' ' + word
                            f_find = True
                            break

                    if f_find:
                        continue

                    f_insert = False
                    for k in range(len(list_index)):
                        if word_pos[1] < list_index[k]:
                            list_index.insert(k, word_pos[1])
                            list_players.insert(k, word)
                            f_insert = True
                            break

                    if not f_insert:
                        list_index.append(word_pos[1])
                        list_players.append(word)

            # === count the kills ===
            list_kills = [0] * len(list_index)
            for j in range(len(block_words)):
                [word, word_pos] = block_words[j]
                if word_pos[0] > 350:
                    if word.isdigit():
                        if int(word) < 10:
                            kills = int(word)
                        else:
                            kills = 1

                        for k in range(len(list_index)):
                            if abs(list_index[k] - word_pos[1]) < 18:
                                list_kills[k] = kills
                    elif word == 'Skills':
                        for k in range(len(list_index)):
                            if abs(list_index[k] - word_pos[1]) < 18:
                                list_kills[k] = 5
                    elif word == 'I' and j + 1 < len(block_words) and block_words[j + 1][0] == "kill":
                        for k in range(len(list_index)):
                            if abs(list_index[k] - word_pos[1]) < 18:
                                list_kills[k] = 1

            # === order number ===
            order = 0
            if i < 3:
                order = i + 1
            else:
                for j in range(len(block_words)):
                    [word, word_pos] = block_words[j]
                    if word_pos[0] < 50 and word.isdigit():
                        order = int(word)

            info = {
                'kills': list_kills,
                'players': list_players,
                'total_kills': sum(list_kills),
                'placement': order,
                'total_points': self.calc_place_score(order) + sum(list_kills)
            }
            ret_info.append(info)
            if debug:
                print(info)

        # ----------------------- fix incorrect order ---------------------------
        for i in range(3, len(ret_info)):
            if i + 1 < len(ret_info) and ret_info[i]['placement'] == 0 and ret_info[i + 1]['placement'] != 0:
                ret_info[i]['placement'] = ret_info[i + 1]['placement'] - 1
                ret_info[i]['total_points'] = self.calc_place_score(ret_info[i]['placement']) + ret_info[i]['total_kills']

        # ------------------------ show debug result -----------------------------
        if debug:
            for i in range(len(block_list)):
                cv2.rectangle(img, tuple(block_list[i][:2]), tuple(block_list[i][2:]), (255, 0, 0), 2)

            cv2.imshow('erode', img_erode)
            cv2.imshow('block', img)

        return ret_info

    @staticmethod
    def compare_players(players1, players2):
        cnt_same = 0
        for i in range(len(players1)):
            if players1[i] in players2:
                cnt_same += 1
            else:
                for j in range(len(players2)):
                    if fuzz.ratio(players1[i], players2[j]) > 70:
                        cnt_same += 1
                        break

        if cnt_same / len(players1) > 0.6:
            return True
        else:
            return False

    def create_excel_data(self, game_data):
        # --------------------- create excel data ------------------------
        excel_data = {}
        for map_ind in range(len(game_data)):
            for team_ind in range(len(game_data[map_ind])):
                team_data = game_data[map_ind][team_ind]

                if not team_data['team'].startswith('Team_') and team_data['team'] in excel_data:
                    excel_data[team_data['team']][map_ind] = team_data
                else:
                    f_dup = False
                    for t_id in excel_data:
                        if map_ind not in excel_data[t_id]:
                            if self.compare_players(team_data['players'], excel_data[t_id]['players']):
                                excel_data[t_id][map_ind] = team_data
                                if not team_data['team'].startswith('Team_'):
                                    excel_data[team_data['team']] = excel_data.pop(t_id)
                                f_dup = True
                                break

                    if not f_dup:
                        if team_data['team'] in excel_data:
                            k = 1
                            while 'Team_' + str(k) in excel_data:
                                k += 1
                            excel_data['Team_' + str(k)] = {'players': team_data['players'], map_ind: team_data}
                        else:
                            excel_data[team_data['team']] = {'players': team_data['players'], map_ind: team_data}

        # --------------------- add total data -----------------------
        for t_id in excel_data:
            total_kills = 0
            total_points = 0
            total_wins = 0
            kills = [0, 0, 0, 0]
            for map_ind in range(len(game_data)):
                if map_ind not in excel_data[t_id]:
                    continue

                total_kills += excel_data[t_id][map_ind]['total_kills']
                total_points += excel_data[t_id][map_ind]['total_points']
                if excel_data[t_id][map_ind]['placement'] == 1:
                    total_wins += 1

                for i in range(len(excel_data[t_id][map_ind]['kills'])):
                    if excel_data[t_id][map_ind]['kills'][i] is not None:
                        kills[i] += excel_data[t_id][map_ind]['kills'][i]

            excel_data[t_id]['Total'] = {
                'total_kills': total_kills,
                'total_points': total_points,
                'total_wins': total_wins,
                'kills': kills
            }

        # ------------------- sort data -------------------------
        final_data = []
        score_list = []
        for t_id in excel_data:
            t_info = excel_data.get(t_id)
            t_info['team'] = t_id
            final_data.append(t_info)
            score_list.append(t_info['Total']['total_points'])

        sort_data = []
        while len(final_data) > 0:
            max_ind = int(np.argmax(score_list))
            sort_data.append(final_data[max_ind])
            final_data.pop(max_ind)
            score_list.pop(max_ind)

        return sort_data

    def save_excel(self, excel_file, excel_data, map_cnt):
        wb = openpyxl.Workbook()
        sheet_names = wb.sheetnames
        sheet = wb[sheet_names[0]]

        # ----------------- title --------------------
        sheet.merge_cells(start_row=1, start_column=1, end_row=2, end_column=1)
        sheet.merge_cells(start_row=1, start_column=2, end_row=2, end_column=2)
        sheet.cell(row=1, column=1).value = "Team"
        sheet.cell(row=1, column=1).alignment = Alignment(horizontal='center')
        sheet.cell(row=1, column=2).value = "Player"
        sheet.cell(row=1, column=2).alignment = Alignment(horizontal='center')

        for i in range(map_cnt):
            sheet.merge_cells(start_row=1, start_column=i * 6 + 3, end_row=1, end_column=i * 6 + 8)
            sheet.cell(row=1, column=i * 6 + 3).value = "Map" + str(i + 1)
            sheet.cell(row=1, column=i * 6 + 3).alignment = Alignment(horizontal='center')
            sheet.cell(row=2, column=i * 6 + 3).value = "Kills"
            sheet.cell(row=2, column=i * 6 + 4).value = "Total Kills"
            sheet.cell(row=2, column=i * 6 + 5).value = "TK Points"
            sheet.cell(row=2, column=i * 6 + 6).value = "Placement"
            sheet.cell(row=2, column=i * 6 + 7).value = "TP Points"
            sheet.cell(row=2, column=i * 6 + 8).value = "Total Points"

            for j in range(6):
                sheet.cell(row=2, column=i * 6 + 3 + j).alignment = Alignment(horizontal='center')

        sheet.merge_cells(start_row=1, start_column=map_cnt * 6 + 3, end_row=1, end_column=map_cnt * 6 + 5)
        sheet.cell(row=1, column=map_cnt * 6 + 3).value = "Total"
        sheet.cell(row=1, column=map_cnt * 6 + 3).alignment = Alignment(horizontal='center')
        sheet.cell(row=2, column=map_cnt * 6 + 3).value = "TK Points"
        sheet.cell(row=2, column=map_cnt * 6 + 4).value = "TP Points"
        sheet.cell(row=2, column=map_cnt * 6 + 5).value = "Total Points"

        for j in range(3):
            sheet.cell(row=2, column=map_cnt * 6 + 3 + j).alignment = Alignment(horizontal='center')

        # -------------- add map info ----------------
        for t_ind in range(len(excel_data)):
            t_data = excel_data[t_ind]
            t4 = t_ind * 4

            # Team and Players name
            sheet.merge_cells(start_row=t4 + 3, start_column=1, end_row=t4 + 6, end_column=1)
            sheet.cell(row=t4 + 3, column=1).value = t_data['team']
            for i in range(len(t_data['players'])):
                sheet.cell(row=t4 + 3 + i, column=2).value = t_data['players'][i]

            # Map info
            for m_ind in range(map_cnt):
                m4 = m_ind * 6
                for i in range(5):
                    sheet.merge_cells(start_row=t4 + 3, start_column=m4 + 4 + i, end_row=t4 + 6, end_column=m4 + 4 + i)

                if m_ind in t_data:
                    sheet.cell(row=t4 + 3, column=m4 + 4).value = t_data[m_ind]['total_kills']
                    sheet.cell(row=t4 + 3, column=m4 + 5).value = t_data[m_ind]['total_kills']
                    sheet.cell(row=t4 + 3, column=m4 + 6).value = t_data[m_ind]['placement']
                    sheet.cell(row=t4 + 3, column=m4 + 7).value = self.calc_place_score(t_data[m_ind]['placement'])
                    sheet.cell(row=t4 + 3, column=m4 + 8).value = t_data[m_ind]['total_points']
                    for i in range(len(t_data[m_ind]['kills'])):
                        sheet.cell(row=t4 + 3 + i, column=m4 + 3).value = t_data[m_ind]['kills'][i]

            # Total info
            m4 = map_cnt * 6
            for i in range(3):
                sheet.merge_cells(start_row=t4 + 3, start_column=m4 + 3 + i, end_row=t4 + 6, end_column=m4 + 3 + i)

            sheet.cell(row=t4 + 3, column=m4 + 3).value = t_data['Total']['total_kills']
            sheet.cell(row=t4 + 3, column=m4 + 4).value = t_data['Total']['total_points'] - t_data['Total']['total_kills']
            sheet.cell(row=t4 + 3, column=m4 + 5).value = t_data['Total']['total_points']

        wb.save(excel_file)
        wb.close()
